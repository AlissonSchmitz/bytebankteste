import 'package:bytebank/models/contact.dart';
import 'package:sqflite/sqflite.dart';

import '../app_database.dart';

class ContactDao {
  static const String _tableName = 'contacts';
  static const String _id = 'id';
  static const String _name = 'name';
  static const String _account = 'account';
  static const String tableContactSql = 'CREATE TABLE $_tableName ('
      '$_id INTEGER PRIMARY KEY, '
      '$_name TEXT, '
      '$_account INTEGER)';

  Future<int> saveContact(Contact contact) async {
    final Database db = await getDatabase();
    return db.insert(_tableName, contact.toMap());
  }

  void deleteContact(Contact contact) {
    getDatabase().then((db) {
      db.delete(_tableName, where: ' $_id = ?', whereArgs: [contact.id]);
    });
  }

  Future<List<Contact>> findAll() async {
    final Database db = await getDatabase();
    final List<Contact> contactList = List();
    final List<Map<String, dynamic>> result = await db.query(_tableName);
    for (Map<String, dynamic> item in result) {
      contactList.add(Contact(
        item[_id],
        item[_name],
        item[_account],
      ));
    }
    return contactList;
  }
}
