class Contact {
  final int id;
  final String name;
  final int account;

  Contact(
    this.id,
    this.name,
    this.account,
  );

  Map<String, dynamic> toMap(){
    return {
      'name': name,
      'account': account,
    };
  }

  @override
  String toString() {
    return 'Contact{id: $id, name: $name, account: $account}';
  }
}
